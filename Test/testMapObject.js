const mapObject = require('../mapObject.js');
const testObject = require('./testobjects.js');

function cb(value) {
    return 'New Value ' + value;
}

const expected = {
    name: 'New Value Bruce Wayne',
    age: 'New Value 36',
    location: 'New Value Gotham'
};

const actual = mapObject(testObject, cb);
console.log (actual);

let res = JSON.stringify (actual) === JSON.stringify (expected);
console.log (res ? "Same" : "Different");
