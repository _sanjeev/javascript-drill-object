const values = require ('./values.js');
const testObject = require ('./testobjects.js');

const actual = values (testObject);
console.log (actual);
const expected = ['Bruce Wayne', 36, 'Gotham'];

let res = false;
for (let index = 0; index < actual.length; index++) {
    if (actual[index] !== expected[index]) {
        console.log ('Different');
        res = true;
    }
}
if (!res) {
    console.log ('Same');
}