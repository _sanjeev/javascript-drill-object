const defaults = require('../defaults.js');
const testObject = require('./testobjects.js');

const defaultProp = {
    address: "HansrajPur",
    pincode: "841222",
};

let actual = defaults(testObject, defaultProp);
console.log(actual);

const expected = {
    name: 'Bruce Wayne',
    age: 36,
    location: 'Gotham',
    address: 'HansrajPur',
    pincode: '841222'
};

const res = JSON.stringify(actual) === JSON.stringify(expected);
console.log(res ? "Same" : "Different");