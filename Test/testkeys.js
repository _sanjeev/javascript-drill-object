const keys = require ('../keys.js');
const testObject = require ('./testobjects.js');
const actual = keys (testObject);
console.log (actual);
const expected = ['name', 'age', 'location'];

let res = false;
for (let index = 0; index < actual.length; index++) {
    if (actual[index] !== expected[index]) {
        console.log ('Different');
        res = true;
    }
}
if (!res) {
    console.log ('Same');
}