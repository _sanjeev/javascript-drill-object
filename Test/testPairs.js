const pairs = require('./../pairs.js');
const testObject = require('./testobjects.js');

const expected = pairs(testObject);
console.log(expected);
const actual = [['name', 'Bruce Wayne'], ['age', 36], ['location', 'Gotham']];

let res = false;
for (let rowIndex = 0; rowIndex < actual.length; rowIndex++) {
    for (let colIndex = 0; colIndex < actual[rowIndex].length; colIndex++) {
        if (actual[rowIndex][colIndex] !== expected[rowIndex][colIndex]) {
            console.log('Different');
            res = true;
        }
    }
}
if (!res) {
    console.log('Same');
}