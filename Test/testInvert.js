const invert = require('../invert.js');
const testObject = require('./testobjects.js');

let actual = invert(testObject);
console.log(actual);
const expected = { '36': 'age', 'Bruce Wayne': 'name', Gotham: 'location' };
let res = JSON.stringify (actual) === JSON.stringify (expected);
console.log (res ? "Same" : "Different");