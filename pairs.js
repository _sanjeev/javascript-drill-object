function pairs (obj) {
    let output = [];
    for (let prop in obj) {
        output.push ([prop, obj[prop]]);
    }
    return output;
}

module.exports = pairs;