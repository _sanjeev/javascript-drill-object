function defaults(obj, defaultProps) {
    let props = [];
    for (let property in defaultProps) {
        props.push (property);
    }
    
    for (let index = 0; index < props.length; index++) {
        if (obj[props[index]] === undefined) {
            obj[props[index]] = defaultProps[props[index]];
        }
    }

    return obj;
}

module.exports = defaults;