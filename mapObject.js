function mapObject (obj, cb) {
    let output = {};
    for (let key in obj) {
        output[key] = cb (obj[key]);
    }
    return output;
}

module.exports = mapObject;