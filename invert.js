function invert (obj) {
    let output = {};
    for (let key in obj) {
        output[obj[key]] = key;
    }
    return output;
}

module.exports = invert;